package com.sfm.cfl.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.sfm.cfl.dto.EntityPage;
import com.sfm.cfl.dto.WagonAllUsers;
import com.sfm.cfl.dto.WagonBoitier;
import com.sfm.cfl.model.Utilisateur;
import com.sfm.cfl.model.Wagon;

public interface WagonService {
	
	Wagon save(Utilisateur userConnected,Wagon wagon);
	
    List<Wagon> findAll();
    
    void delete(Utilisateur userConnected,long id);
    
    Wagon findById(Long id);
    
    EntityPage<Wagon> findByKeyword(Utilisateur userConnected,String keyword, Pageable pageable);
	
	
	WagonAllUsers follow(Utilisateur userConnected, Wagon wagon, Utilisateur utilisateur);
	WagonAllUsers UnFollow(Utilisateur userConnected, Wagon wagon, Utilisateur utilisateur);
	WagonAllUsers followers(Utilisateur userConnected, Wagon wagon, String recherche);

	EntityPage<Wagon> pairingWagon(Utilisateur userConnected, WagonBoitier wagonBoitier);

	
}
