package com.sfm.cfl.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.sfm.cfl.dto.EntityPage;
import com.sfm.cfl.dto.UserDto;
import com.sfm.cfl.enumer.UserRoles;
import com.sfm.cfl.model.Utilisateur;

import javax.mail.MessagingException;

public interface UserService {

	Utilisateur save(UserDto userDto, Utilisateur userConnected);
    List<Utilisateur> findAll();
    void delete(Utilisateur userConnected, long id);
    Utilisateur findByEmail(String email);
    
    Utilisateur changePassword(Utilisateur userConnected, Utilisateur userToChange);
    Utilisateur enable(Utilisateur userConnected, Utilisateur user);

    Utilisateur findById(Utilisateur userConnected, long id);
    long countByRole(UserRoles role);
    
    List<Utilisateur> findByRole(UserRoles role);
    
    EntityPage<Utilisateur> findByNomContaining(Utilisateur userConnected, String nom, Pageable pageable);
    EntityPage<Utilisateur> findByRoleAndKeyword(Utilisateur userConnected, UserRoles role, String keyword, Pageable pageable);

    boolean passwordResetRequest(String email) throws MessagingException;
    boolean passwordReset(String token, String password);
}
