package com.sfm.cfl.service;

import com.sfm.cfl.dto.EntityPage;
import com.sfm.cfl.model.TypeEtat;
import com.sfm.cfl.model.Utilisateur;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface TypeEtatService {

    TypeEtat save(Utilisateur userConnected, TypeEtat typeEtat);

    List<TypeEtat> findAll();

    void delete(Utilisateur userConnected,long id);

    TypeEtat findById(Long id);

    EntityPage<TypeEtat> findByKeyword(String recherche, Pageable pageable);

}
