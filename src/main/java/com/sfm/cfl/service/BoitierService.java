package com.sfm.cfl.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.sfm.cfl.dto.BoitierEntreprise;
import com.sfm.cfl.dto.BoitierParametreValeur;
import com.sfm.cfl.dto.BoitierSim;
import com.sfm.cfl.dto.EntityPage;
import com.sfm.cfl.enumer.PairingBox;
import com.sfm.cfl.model.Boitier;
import com.sfm.cfl.model.Utilisateur;
import com.sfm.cfl.model.ValeurParametre;

public interface BoitierService {
	
	Boitier newBoitier(Boitier boitier, String versionHardware);
	
	Boitier saveBoitier(Boitier boitier);
	
    List<Boitier> findAll();
    
    void delete(Utilisateur userConnected,long id);
    
    Boitier findById(Long id);
    
    Boitier findByImei(String imei);
    
    EntityPage<Boitier> findByKeyword(Utilisateur userConnected, String keyword, Pageable pageable);

	EntityPage<Boitier> findByKeywordAndEtat(Utilisateur userConnected, String keyword, PairingBox etat,
			Pageable pageable);

	List<ValeurParametre> findBoitierParametre(Utilisateur userConnected,Boitier boitier);
	List<ValeurParametre> findBoitierParametre(Boitier boitier);
	ValeurParametre setBoitierParametre(Boitier boitier, long paramId, String valeur);

	List<ValeurParametre> updateBoitierParametre(Utilisateur userConnected,
			BoitierParametreValeur boitierParametreValeur);

	EntityPage<Boitier> affecteToEntreprise(Utilisateur userConnected, BoitierEntreprise boitierEntreprise);

	Boitier changeSim(BoitierSim boitierSim);
	
	Boitier changeEtatNotif(BoitierSim boitierSim);
}
