package com.sfm.cfl.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;

import com.sfm.cfl.dto.EntityPage;
import com.sfm.cfl.dto.mesures.CountParBoitier;
import com.sfm.cfl.dto.mesures.IndicateurMesures;
import com.sfm.cfl.dto.mesures.LastMesure;
import com.sfm.cfl.model.Mesure;
import com.sfm.cfl.model.Utilisateur;

public interface MesureService {

    Mesure save(Mesure mesure);

    List<Mesure> findAll();

    void delete(long id);

    Mesure findById(Long id);

    EntityPage<Mesure> listMesure(long boitier_id, long indicateur_id, Pageable pageable);

	List<Mesure> listMesureByDate(long boitier_id, long indicateur_id, Date debut, Date fin);

	LastMesure lastMesures(long idBoitier);

	List<IndicateurMesures> allMesuresByDate(long idBoitier, Date dateDebut, Date dateFin);

	List<CountParBoitier> countEnvoiParBoitier(Utilisateur userConnected, Date dateDebut, Date dateFin);

}
