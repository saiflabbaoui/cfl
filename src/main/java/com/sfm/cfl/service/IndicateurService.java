package com.sfm.cfl.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.sfm.cfl.dto.EntityPage;
import com.sfm.cfl.model.Indicateur;
import com.sfm.cfl.model.Utilisateur;

public interface IndicateurService {
	
	Indicateur save(Utilisateur userConnected, Indicateur indicateur);
	
    List<Indicateur> findAll();
    
    void delete(Utilisateur userConnected,long id);
    
    Indicateur findById(Long id);
    
    EntityPage<Indicateur> findByKeyword(String keyword, Pageable pageable);
}
