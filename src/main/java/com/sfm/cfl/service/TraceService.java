package com.sfm.cfl.service;

import java.util.Date;
import java.util.List;

import com.sfm.cfl.model.Trace;
import com.sfm.cfl.model.Utilisateur;

public interface TraceService {
	
	Trace save(Trace trace);
	
    List<Trace> findAll();
    
    void delete(long id);
    
    Trace findById(Long id);
    
    long count();
    
    List<Trace> findAllByUserIdAndDateBetween(Utilisateur userConnected, long id, Date debut, Date fin);
    
}
