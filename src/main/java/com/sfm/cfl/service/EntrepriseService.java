package com.sfm.cfl.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.sfm.cfl.dto.EntityPage;
import com.sfm.cfl.dto.EntrepriseDto;
import com.sfm.cfl.model.Entreprise;
import com.sfm.cfl.model.Utilisateur;

public interface EntrepriseService {
	
	Entreprise save(Utilisateur userConnected,EntrepriseDto entreprise);
	
    List<Entreprise> findAll();
    
    void delete(Utilisateur userConnected,long id);
    
    Entreprise findById(Long id);
    
    EntityPage<Entreprise> findByKeyword(String keyword, Pageable pageable);
}
