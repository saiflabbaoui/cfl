package com.sfm.cfl.service;

import com.sfm.cfl.dto.EntityPage;
import com.sfm.cfl.model.SuiviWagon;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface SuiviWagonService {

    SuiviWagon save(SuiviWagon suiviWagon);

    List<SuiviWagon> findAll();

    void delete(long id);

    SuiviWagon findById(Long id);

    EntityPage<SuiviWagon> listSuiviWagon(long user_id, long wagon_id, Pageable pageable);
}
