package com.sfm.cfl.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.sfm.cfl.dto.EntityPage;
import com.sfm.cfl.model.Boitier;
import com.sfm.cfl.model.Parametre;
import com.sfm.cfl.model.Utilisateur;
import com.sfm.cfl.model.ValeurParametre;

public interface ParametreService {
	
	Parametre save(Utilisateur userConnected,Parametre parametre);
	
    List<Parametre> findAll();
    
    void delete(Utilisateur userConnected,long id);
    
    Parametre findById(Long id);
    
    EntityPage<Parametre> findByKeyword(String keyword, Pageable pageable);

	ValeurParametre findParamValueByBoitier(long parametreId, Boitier boitier);
    
    
}
