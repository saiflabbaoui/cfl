package com.sfm.cfl.service;

import java.util.Date;
import java.util.List;

import com.sfm.cfl.dto.mesures.Alarme;
import com.sfm.cfl.dto.mesures.AlarmeParBoitier;
import com.sfm.cfl.model.Boitier;
import com.sfm.cfl.model.Etat;
import com.sfm.cfl.model.Utilisateur;

public interface EtatService {

    Etat save(Etat etat);

    List<Etat> findAll();

    void delete(long id);

    Etat findById(Long id);
    
    Etat findByBoitier(Boitier boitier);

	List<Alarme> allEtatsByDate(long idBoitier, Date dateDebut, Date dateFin);

	List<Alarme> allAlarmesByDate(long idBoitier, Date dateDebut, Date dateFin, Boolean resolu);

	List<Alarme> alarmeResolved(Utilisateur userConnected, long idEtat);

	long alarmeCount(Utilisateur connectedUser);

	List<Alarme> allAlarmes(Utilisateur userConnected, Boolean resolu);
	
	List<AlarmeParBoitier> lastAlarmeParBox(Utilisateur userConnected, Boolean resolu);
}
