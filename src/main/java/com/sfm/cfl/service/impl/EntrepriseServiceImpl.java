package com.sfm.cfl.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.sfm.cfl.dao.EntrepriseDao;
import com.sfm.cfl.dao.TraceDao;
import com.sfm.cfl.dao.UserDao;
import com.sfm.cfl.dto.EntityPage;
import com.sfm.cfl.dto.EntrepriseDto;
import com.sfm.cfl.dto.PageUtil;
import com.sfm.cfl.dto.UserDto;
import com.sfm.cfl.enumer.UserRoles;
import com.sfm.cfl.exception.EntityNotFoundException;
import com.sfm.cfl.model.Entreprise;
import com.sfm.cfl.model.Trace;
import com.sfm.cfl.model.Utilisateur;
import com.sfm.cfl.service.EntrepriseService;


@Service(value = "entrepriseService")
public class EntrepriseServiceImpl implements EntrepriseService {
	
	@Autowired
	private EntrepriseDao entrepriseDao;
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private TraceDao traceDao;
	
	@Autowired
	private BCryptPasswordEncoder bcryptEncoder;

	@Override
	public Entreprise save(Utilisateur userConnected,EntrepriseDto entrepriseDto) {
		
		
		// Préparation de l'action pour la traçabilité
		String action;
		if(entrepriseDto.getId() == 0) {
    		action = "Ajout entreprise : " + System.lineSeparator();
    	} else {
    		action = "Modification entreprise : " +  System.lineSeparator();
    		Entreprise oldEntreprise = entrepriseDao.findById(entrepriseDto.getId()).get();
    		// Traces de l'ancien
			action += oldEntreprise.toString() + " ==> " + System.lineSeparator();
    	}			
		
		
		UserDto gestionnaire = entrepriseDto.getGestionnaire();
		
		//Créer l'entreprise
		Entreprise entreprise = new Entreprise(entrepriseDto.getId(), entrepriseDto.getNom(), entrepriseDto.getLogo());
		entreprise = entrepriseDao.save(entreprise);
		
		//Créer l'utilisateur
		Utilisateur user;
		
		if (gestionnaire.getId() == 0) { // Nouvel utilisateur
			user = new Utilisateur(gestionnaire);
			user.setPassword(bcryptEncoder.encode(gestionnaire.getPassword()));
		} else {
			// Utilisateur existant
			user = userDao.findById(gestionnaire.getId()).get();
			String password = user.getPassword();
			user = new Utilisateur(gestionnaire);
			user.setPassword(password);
		}
		user.setEntreprise(entreprise);
		user.setRole(UserRoles.Administrateur);
		user = userDao.save(user);
		
		// Spécifier que le gestionnaire est également cet utilisateur
		entreprise.setGestionnaire(user);
		entreprise = entrepriseDao.save(entreprise);
		
		entrepriseDao.refresh(entreprise);
		
		// Traces du nouveau
		action += entreprise.toString();
		
		// Sauvegarder la trace
		traceDao.save(new Trace(new Date(), action, userConnected));
		
		return entreprise;
	}
	
	@Override
	public List<Entreprise> findAll() {
		List<Entreprise> list = new ArrayList<>();
		entrepriseDao.findAll().iterator().forEachRemaining(list::add);
		return list;
	}

	@Override
	public void delete(Utilisateur userConnected,long id) {
		Entreprise entreprise = findById(id);
		
		// Préparation de l'action pour la traçabilité
		String action = "Suppression entreprise : " + System.lineSeparator() + entreprise.toString();
		
		entrepriseDao.deleteById(id);
		
		// Sauvegarder la trace
		traceDao.save(new Trace(new Date(), action, userConnected));
	}
	
	@Override
	public Entreprise findById(Long id) {
		Entreprise entreprise =  entrepriseDao.findById(id)
				.orElseGet(() -> entrepriseDao.findById(id)
				.orElseThrow(() -> new EntityNotFoundException("Entreprise introuvable")));
		
		return entreprise;
	}
	
	@Override
	public EntityPage<Entreprise> findByKeyword(String keyword, Pageable pageable) {
		
		Page<Entreprise> entreprisesPage = entrepriseDao.findByNomContaining(keyword,pageable);

		EntityPage<Entreprise> entreprises = new EntityPage<Entreprise>();

		entreprises.setList(entreprisesPage.getContent());

		PageUtil pageUtil = new PageUtil();
		pageUtil.setNombreElementParPage(entreprisesPage.getNumberOfElements());
		pageUtil.setNombrePage(entreprisesPage.getTotalPages());
		pageUtil.setNumeroPage(entreprisesPage.getNumber() + 1);
		pageUtil.setNombreTotalElement(entreprisesPage.getTotalElements());

		entreprises.setPageUtil(pageUtil);

		return entreprises;
	}


}
