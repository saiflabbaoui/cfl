package com.sfm.cfl.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sfm.cfl.dao.EtatDao;
import com.sfm.cfl.dao.TraceDao;
import com.sfm.cfl.dto.mesures.Alarme;
import com.sfm.cfl.dto.mesures.AlarmeParBoitier;
import com.sfm.cfl.model.Boitier;
import com.sfm.cfl.model.Etat;
import com.sfm.cfl.model.Trace;
import com.sfm.cfl.model.Utilisateur;
import com.sfm.cfl.service.EtatService;

@Service(value = "etatService")
public class EtatServiceImpl implements EtatService {

    @Autowired
    private EtatDao etatDao;
    
    @Autowired
    private TraceDao traceDao;

    @Override
    public Etat save(Etat etat) {
        etat = etatDao.save(etat);
        etatDao.refresh(etat);
        return etat;
    }

    @Override
    public List<Etat> findAll() {
        List<Etat> list = new ArrayList<>();
        etatDao.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public void delete(long id) {
        etatDao.deleteById(id);
    }

    @Override
    public Etat findById(Long id) {
        return etatDao.findById(id).get();
    }
    
    @Override
    public Etat findByBoitier(Boitier boitier) {
    	return etatDao.findTopByBoitierOrderByIdDesc(boitier);
    }
    
    @Override
    public List<Alarme> allEtatsByDate(long idBoitier, Date dateDebut, Date dateFin) {
    	return etatDao.findByBoitier(idBoitier, dateDebut, dateFin);
    }
    
    @Override
    public List<Alarme> allAlarmesByDate(long idBoitier, Date dateDebut, Date dateFin, Boolean resolu) {
    	return etatDao.findAlarmesByDate(idBoitier, dateDebut, dateFin, resolu);
    }
    
	@Override
    public List<Alarme> allAlarmes(Utilisateur userConnected, Boolean resolu) {
		
		
		switch (userConnected.getRole()) {
		case SuperAdministrateur:
			// Retournez toutes les alarmes
			return etatDao.allAlarmes(false, true);
			
		case Administrateur:			
			//Lui donner les alarmes de son entreprise
			return etatDao.alarmesByEntreprise(userConnected.getEntreprise(), false, true);
			
		case Utilisateur:
			//Lui retourner les alarmes des wagons qu'il suit
			return etatDao.userAlarme(userConnected, resolu, true);

		default:
			return null;
		}
    }
	
	@Override
    public List<AlarmeParBoitier> lastAlarmeParBox(Utilisateur userConnected, Boolean resolu) {
		
		
		switch (userConnected.getRole()) {
		case SuperAdministrateur:
			// Retournez toutes les alarmes
			return etatDao.lastAlarmes(false, true);
			
		case Administrateur:			
			//Lui donner les alarmes de son entreprise
			return etatDao.lastAlarmesByEntreprise(userConnected.getEntreprise(), false, true);
			
		case Utilisateur:
			//Lui retourner les alarmes des wagons qu'il suit
			return etatDao.lastUserAlarme(userConnected, resolu, true);

		default:
			return null;
		}
    }
    
    @Override
    public List<Alarme> alarmeResolved(Utilisateur userConnected, long idEtat) {
    	Etat etat = etatDao.findById(idEtat).get();
    	
    	// If already resolved le dire
    	
    	
    	etat.setResolu(true);
    	etat = etatDao.save(etat);
    	
    	// Enregistrer une trace de la résolution
    	traceDao.save(new Trace(new Date(), "Résolution d'alarme ", userConnected, etat));
    	
    	return allAlarmes(userConnected, false);
    }
    
    @Override
    public long alarmeCount(Utilisateur connectedUser) {
    	
    	switch (connectedUser.getRole()) {
		case SuperAdministrateur:
			// Retournez toutes les alarmes
			return etatDao.countByResoluAndEtatAlarme(false, true);
			
		case Administrateur:			
			//Lui donner le nombre d'alarmes de son entreprise
			return etatDao.countByBoitierEntrepriseAndResoluAndEtatAlarme(connectedUser.getEntreprise(), false, true);
			
		case Utilisateur:
			//Lui donner le nombre d'alarmes des wagons qu'il suit
			return etatDao.countUserAlarme(connectedUser, false, true);

		default:
			return 0;
		}
    
    }
}
