package com.sfm.cfl.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sfm.cfl.dao.BoitierDao;
import com.sfm.cfl.dao.TimelineBoxDao;
import com.sfm.cfl.enumer.UserRoles;
import com.sfm.cfl.exception.NotAuthorizedException;
import com.sfm.cfl.model.Boitier;
import com.sfm.cfl.model.TimelineBox;
import com.sfm.cfl.model.Utilisateur;
import com.sfm.cfl.service.TimelineBoxService;

@Service(value = "timelineBoxService")
public class TimelineBoxServiceImpl implements TimelineBoxService {

    @Autowired
    private TimelineBoxDao timelineBoxDao;
    
    @Autowired
    private BoitierDao boitierDao;
    
    @Override
    public List<TimelineBox> findAllByBoitierId(Utilisateur userConnected, 
    		long boitierId) {
    	
    	if (userConnected.getRole() == UserRoles.SuperAdministrateur) {
    		// Il a le droit
		} else if (userConnected.getRole() == UserRoles.Administrateur) {
			Boitier boitier = boitierDao.findById(boitierId).get();
			//S'assurer que c'est un utilisateur de la même entreprise
			if (boitier.getEntreprise().getId() != userConnected.getEntreprise().getId()) {
				throw new NotAuthorizedException();
			}
		} else {
			throw new NotAuthorizedException();
		}
    	
    	return timelineBoxDao.findAllByBoitierId(boitierId);
    }
    
    

}
