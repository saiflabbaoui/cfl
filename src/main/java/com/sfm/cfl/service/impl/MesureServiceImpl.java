package com.sfm.cfl.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.sfm.cfl.dao.IndicateurDao;
import com.sfm.cfl.dao.MesureDao;
import com.sfm.cfl.dao.TypeEtatDao;
import com.sfm.cfl.dto.EntityPage;
import com.sfm.cfl.dto.PageUtil;
import com.sfm.cfl.dto.mesures.CountParBoitier;
import com.sfm.cfl.dto.mesures.EtatDTO;
import com.sfm.cfl.dto.mesures.IndicateurMesures;
import com.sfm.cfl.dto.mesures.LastMesure;
import com.sfm.cfl.dto.mesures.MesureDTOLibelle;
import com.sfm.cfl.dto.mesures.MesureDTOVal;
import com.sfm.cfl.model.Indicateur;
import com.sfm.cfl.model.Mesure;
import com.sfm.cfl.model.TypeEtat;
import com.sfm.cfl.model.Utilisateur;
import com.sfm.cfl.service.MesureService;

@Service(value = "mesureService")
public class MesureServiceImpl implements MesureService {

    @Autowired
    private MesureDao mesureDao;
    
    @Autowired
    private IndicateurDao indicateurDao;
    
    @Autowired
    private TypeEtatDao typeEtatDao;
    
    @Value("${indic.EtatWagon}")
	private long indicEtatWagon;

    @Override
    public Mesure save(Mesure mesure) {
        mesure = mesureDao.save(mesure);
        mesureDao.refresh(mesure);
        return mesure;
    }

    @Override
    public List<Mesure> findAll() {
        List<Mesure> list = new ArrayList<>();
        mesureDao.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public void delete(long id) {
        mesureDao.deleteById(id);
    }

    @Override
    public Mesure findById(Long id) {
        return mesureDao.findById(id).get();
    }

    @Override
    public EntityPage<Mesure> listMesure(long boitier_id, long indicateur_id, Pageable pageable) {
        Page<Mesure> mesuresPage = mesureDao.findByBoitierIdAndIndicateurId(boitier_id, indicateur_id, pageable);

        EntityPage<Mesure> mesures = new EntityPage<Mesure>();

        mesures.setList(mesuresPage.getContent());

        PageUtil pageUtil = new PageUtil();
        pageUtil.setNombreElementParPage(mesuresPage.getNumberOfElements());
        pageUtil.setNombrePage(mesuresPage.getTotalPages());
        pageUtil.setNumeroPage(mesuresPage.getNumber() + 1);
        pageUtil.setNombreTotalElement(mesuresPage.getTotalElements());

        mesures.setPageUtil(pageUtil);

        return mesures;
    }
    
	@Override
    public List<Mesure> listMesureByDate(long boitier_id, long indicateur_id, Date debut, Date fin) {
        List<Mesure> mesures = mesureDao.findByBoitierIdAndIndicateurIdAndDateBetweenOrderByDate(boitier_id, indicateur_id, debut, fin);
        return mesures;
    }
	
	@Override
	public LastMesure lastMesures(long idBoitier) {
		
		LastMesure retour = new LastMesure();
		List<MesureDTOLibelle> retourList = new ArrayList<>();
		
		List<Indicateur> indicateurs = indicateurDao.findAll();
		if (!indicateurs.isEmpty())
		for (Indicateur indicateur : indicateurs) {
			Mesure mesure = mesureDao.findTopByBoitierIdAndIndicateurIdOrderByIdDesc(idBoitier, indicateur.getId());
			if (mesure != null) {
				MesureDTOLibelle m = new MesureDTOLibelle(
						mesure.getVal1(), mesure.getVal2(), mesure.getVal3(),
						indicateur.getId(),indicateur.getLibelle());
				//Récupération de l'état
				if (indicateur.getId() == indicEtatWagon) {
					TypeEtat typeEtat = typeEtatDao.findById(Long.parseLong(mesure.getVal1())).get(); 
					retour.setEtat(new EtatDTO(typeEtat.getId(), typeEtat.getLibelle()));
				}
				retourList.add(m);
			}
		}
		
		retour.setMesures(retourList);
		
		return retour;
		
	}
	
//	@Override
//	public List<IndicateurMesures> allMesuresByDate(long idBoitier, Date dateDebut, Date dateFin){
//		
//		List<IndicateurMesures> retour = new ArrayList<>();
//		
//		List<Indicateur> indicateurs = indicateurDao.findAll();
//		if (!indicateurs.isEmpty())
//		for (Indicateur indicateur : indicateurs) {
//			List<MesureDTOVal> dtoVals = mesureDao.findByBoitierAndIndicateur(idBoitier, indicateur.getId(), dateDebut, dateFin);
//			if (!dtoVals.isEmpty()) {
//				retour.add(new IndicateurMesures(indicateur,dtoVals));
//			}
//		}
//		
//		return retour;
//		
//	}
	
	@Override
	public List<IndicateurMesures> allMesuresByDate(long idBoitier, Date dateDebut, Date dateFin){
		
		List<IndicateurMesures> retour = new ArrayList<>();
		
		List<Indicateur> indicateurs = indicateurDao.findAll();
		if (!indicateurs.isEmpty())
		for (Indicateur indicateur : indicateurs) {
			
			List<MesureDTOVal> mesures = new ArrayList<>();
			
			List<?> dtoVals1 = mesureDao.findByBoitierAndIncateurVal1(idBoitier, indicateur.getId(), dateDebut, dateFin);
			if (!dtoVals1.isEmpty()) {
				mesures.add(new MesureDTOVal(dtoVals1,indicateur.getLibelle()));
			}
			
			List<?> dtoVals2 = mesureDao.findByBoitierAndIncateurVal2(idBoitier, indicateur.getId(), dateDebut, dateFin);
			if (!dtoVals2.isEmpty()) {
				mesures.add(new MesureDTOVal(dtoVals2,indicateur.getLibelle()));
			}
			
			List<?> dtoVals3 = mesureDao.findByBoitierAndIncateurVal3(idBoitier, indicateur.getId(), dateDebut, dateFin);
			if (!dtoVals3.isEmpty()) {
				mesures.add(new MesureDTOVal(dtoVals3,indicateur.getLibelle()));
			}
			
			retour.add(new IndicateurMesures(indicateur, mesures));
		}
		
		return retour;
		
	}
	
	
	@Override
	public List<CountParBoitier> countEnvoiParBoitier(Utilisateur userConnected, Date dateDebut, Date dateFin) {
		
		switch (userConnected.getRole()) {
		case SuperAdministrateur:
			// Retourner pour tous les boitiers
			return mesureDao.countParBoitier(dateDebut, dateFin);
			
		case Administrateur:			
			//Lui donner les alarmes de son entreprise
			return mesureDao.countParBoitierAndEntreprise(userConnected.getEntreprise(), dateDebut, dateFin);
			
		case Utilisateur:
			//Lui retourner les alarmes des wagons qu'il suit
			return mesureDao.countParBoitierForUser(userConnected, dateDebut, dateFin);

		default:
			return null;
		}
		
		
	}
}
