package com.sfm.cfl.service.impl;

import com.sfm.cfl.dao.SuiviWagonDao;
import com.sfm.cfl.dto.EntityPage;
import com.sfm.cfl.dto.PageUtil;
import com.sfm.cfl.model.SuiviWagon;
import com.sfm.cfl.service.SuiviWagonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service(value = "suiviWagonService")
public class SuiviWagonServiceImpl implements SuiviWagonService {

    @Autowired
    private SuiviWagonDao suiviWagonDao;


    @Override
    public SuiviWagon save(SuiviWagon suiviWagon) {
        suiviWagon = suiviWagonDao.save(suiviWagon);
        suiviWagonDao.refresh(suiviWagon);
        return suiviWagon;
    }

    @Override
    public List<SuiviWagon> findAll() {
        List<SuiviWagon> list = new ArrayList<>();
        suiviWagonDao.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public void delete(long id) {
        suiviWagonDao.deleteById(id);
    }

    @Override
    public SuiviWagon findById(Long id) {
        return suiviWagonDao.findById(id).get();
    }

    @Override
    public EntityPage<SuiviWagon> listSuiviWagon(long user_id, long wagon_id, Pageable pageable) {
        Page<SuiviWagon> suiviWagonsPage = suiviWagonDao.findByUserIdAndWagonId(user_id, wagon_id, pageable);

        EntityPage<SuiviWagon> suiviWagonEntityPage = new EntityPage<SuiviWagon>();

        suiviWagonEntityPage.setList(suiviWagonsPage.getContent());

        PageUtil pageUtil = new PageUtil();
        pageUtil.setNombreElementParPage(suiviWagonsPage.getNumberOfElements());
        pageUtil.setNombrePage(suiviWagonsPage.getTotalPages());
        pageUtil.setNumeroPage(suiviWagonsPage.getNumber() + 1);
        pageUtil.setNombreTotalElement(suiviWagonsPage.getTotalElements());

        suiviWagonEntityPage.setPageUtil(pageUtil);

        return suiviWagonEntityPage;
    }
}
