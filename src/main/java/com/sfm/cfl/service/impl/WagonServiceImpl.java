package com.sfm.cfl.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.sfm.cfl.dao.BoitierDao;
import com.sfm.cfl.dao.SuiviWagonDao;
import com.sfm.cfl.dao.TimelineBoxDao;
import com.sfm.cfl.dao.TraceDao;
import com.sfm.cfl.dao.UserDao;
import com.sfm.cfl.dao.WagonDao;
import com.sfm.cfl.dto.EntityPage;
import com.sfm.cfl.dto.PageUtil;
import com.sfm.cfl.dto.WagonAllUsers;
import com.sfm.cfl.dto.WagonBoitier;
import com.sfm.cfl.enumer.PairingBox;
import com.sfm.cfl.enumer.UserRoles;
import com.sfm.cfl.exception.ErrorException;
import com.sfm.cfl.exception.NotAuthorizedException;
import com.sfm.cfl.model.Boitier;
import com.sfm.cfl.model.SuiviWagon;
import com.sfm.cfl.model.TimelineBox;
import com.sfm.cfl.model.Trace;
import com.sfm.cfl.model.Utilisateur;
import com.sfm.cfl.model.Wagon;
import com.sfm.cfl.service.WagonService;


@Service(value = "wagonService")
public class WagonServiceImpl implements WagonService {
	
	@Autowired
	private WagonDao wagonDao;
	
	@Autowired
	private SuiviWagonDao suiviWagonDao;
	
	@Autowired
	private TraceDao traceDao;
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private BoitierDao boitierDao;
	
	@Autowired
	private TimelineBoxDao timelineBoxDao;

	@Override
	public Wagon save(Utilisateur userConnected,Wagon wagon) {
		
		// Préparation de l'action pour la traçabilité
		String action;
		if(wagon.getId() == 0) {
    		action = "Ajout wagon : " + System.lineSeparator();
    	} else {
    		action = "Modification wagon : " +  System.lineSeparator();
    		Wagon oldWagon = wagonDao.findById(wagon.getId()).get();
    		// Traces de l'ancien
			action += oldWagon.toString() + " ==> " + System.lineSeparator();
			
			//Garder les boitiers au cas où il y'en aurait
			wagon.setBoitiers(oldWagon.getBoitiers());
    	}		
		
		wagon.setEntreprise(userConnected.getEntreprise());
		wagon = wagonDao.save(wagon);
		wagonDao.refresh(wagon);
		
		// Traces du nouveau
		action += wagon.toString();
				
		// Sauvegarder la trace
		traceDao.save(new Trace(new Date(), action, userConnected));
		
		return wagon;
	}

	@Override
	public List<Wagon> findAll() {
		List<Wagon> list = new ArrayList<>();
		wagonDao.findAll().iterator().forEachRemaining(list::add);
		return list;
	}

	@Override
	public void delete(Utilisateur userConnected,long id) {
		Wagon wagon = findById(id);
		
		// Préparation de l'action pour la traçabilité
		String action = "Suppression wagon : " + System.lineSeparator() + wagon.toString();
		
		wagonDao.deleteById(id);
		
		// Sauvegarder la trace
		traceDao.save(new Trace(new Date(), action, userConnected));
	}

	@Override
	public Wagon findById(Long id) {
		return wagonDao.findById(id).get();
	}
	
	@Override
	public EntityPage<Wagon> findByKeyword(Utilisateur userConnected,String keyword, Pageable pageable) {
		
		
		
		if ((userConnected.getRole() == UserRoles.SuperAdministrateur) ||
		(userConnected.getRole() == UserRoles.Administrateur)){
			
			Page<Wagon> wagonsPage;
			
			if (userConnected.getRole() == UserRoles.SuperAdministrateur) {
				wagonsPage = wagonDao.findBySerieContaining(keyword,pageable);
			} else {
				wagonsPage = wagonDao.findByEntrepriseAndSerieContaining(userConnected.getEntreprise(),keyword,pageable);
			}

			EntityPage<Wagon> wagons = new EntityPage<Wagon>();

			wagons.setList(wagonsPage.getContent());

			PageUtil pageUtil = new PageUtil();
			pageUtil.setNombreElementParPage(wagonsPage.getNumberOfElements());
			pageUtil.setNombrePage(wagonsPage.getTotalPages());
			pageUtil.setNumeroPage(wagonsPage.getNumber() + 1);
			pageUtil.setNombreTotalElement(wagonsPage.getTotalElements());

			wagons.setPageUtil(pageUtil);

			return wagons;
			
		} else {
			
			// Lui afficher juste les wagons qu'il suit
			Pageable pageable2 = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by("wagon.serie").ascending());
			Page<SuiviWagon> suiviWagonsPage = suiviWagonDao.findByUserIdAndWagonSerieContaining(userConnected.getId(),keyword,pageable2);

			EntityPage<Wagon> wagons = new EntityPage<Wagon>();
			
			List<Wagon> wagonsList =  new ArrayList<>();
			if (!suiviWagonsPage.getContent().isEmpty())
			for (SuiviWagon suiviWagon : suiviWagonsPage.getContent()) {
				wagonsList.add(suiviWagon.getWagon());
			}

			wagons.setList(wagonsList);

			PageUtil pageUtil = new PageUtil();
			pageUtil.setNombreElementParPage(suiviWagonsPage.getNumberOfElements());
			pageUtil.setNombrePage(suiviWagonsPage.getTotalPages());
			pageUtil.setNumeroPage(suiviWagonsPage.getNumber() + 1);
			pageUtil.setNombreTotalElement(suiviWagonsPage.getTotalElements());

			wagons.setPageUtil(pageUtil);

			return wagons;
			
		}
		
	}
	
	@Override
	public WagonAllUsers follow(Utilisateur userConnected, Wagon wagon, Utilisateur utilisateur) {
		
		wagon = findById(wagon.getId());
		
		if (userConnected.getRole() == UserRoles.Administrateur) {
			// S'assurer que le wagon est de son entreprise
			if (wagon.getEntreprise().getId() != userConnected.getEntreprise().getId()) {
				throw new NotAuthorizedException();
			}

				try {
					
					utilisateur = userDao.findById(utilisateur.getId()).get();
					
					if (utilisateur.getEntreprise().getId() != userConnected.getEntreprise().getId()) 
						throw new NotAuthorizedException();
					
					suiviWagonDao.save(new SuiviWagon(utilisateur, wagon));
					
					// Préparation de l'action pour la traçabilité
					String action;
					action = "Suivi wagon : " + System.lineSeparator();
		    		action += "Wagon : " + wagon.toString() + System.lineSeparator();
		    		action += "Utilisateur : " + utilisateur.toString();
					// Sauvegarder la trace
					traceDao.save(new Trace(new Date(), action, userConnected));
					
				} catch (Exception e) {
					e.printStackTrace();
				}

			
		} else {
			throw new NotAuthorizedException();
		}
		
		return followers(userConnected, wagon,"");
	}
	
	@Override
	public WagonAllUsers UnFollow(Utilisateur userConnected, Wagon wagon, Utilisateur utilisateur) {
		
		wagon = findById(wagon.getId());
		
		if (userConnected.getRole() == UserRoles.Administrateur) {
			// S'assurer que le wagon est de son entreprise
			if (wagon.getEntreprise().getId() != userConnected.getEntreprise().getId()) {
				throw new NotAuthorizedException();
			}

				try {
					
					utilisateur = userDao.findById(utilisateur.getId()).get();
					
					if (utilisateur.getEntreprise().getId() != userConnected.getEntreprise().getId())
						throw new NotAuthorizedException();
					
					SuiviWagon sw = suiviWagonDao.findByUserIdAndWagonId(
							utilisateur.getId(), wagon.getId());
					suiviWagonDao.delete(sw);
					
					// Préparation de l'action pour la traçabilité
					String action;
					action = "Arrêt de Suivi wagon : " + System.lineSeparator();
		    		action += "Wagon : " + wagon.toString() + System.lineSeparator();
		    		action += "Utilisateur : " + utilisateur.toString();
					// Sauvegarder la trace
					traceDao.save(new Trace(new Date(), action, userConnected));
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			
		} else {
			throw new NotAuthorizedException();
		}
		
		return followers(userConnected, wagon,"");
	}
	
	@Override
	public WagonAllUsers followers(Utilisateur userConnected, Wagon wagon, String recherche) {
		
		wagon = findById(wagon.getId());
		
		if (userConnected.getRole() == UserRoles.SuperAdministrateur) {
			//Il a le droit
		} else if (userConnected.getRole() == UserRoles.Administrateur) {
			// S'assurer que le wagon est de son entreprise
			if (wagon.getEntreprise().getId() != userConnected.getEntreprise().getId()) {
				throw new NotAuthorizedException();
			}		
		} else {
			throw new NotAuthorizedException();
		}
		
		List<Utilisateur> followers =  new ArrayList<>();
		//List<SuiviWagon> suiviWagons =  suiviWagonDao.findByWagon(wagon);
		List<SuiviWagon> suiviWagons =  suiviWagonDao.findByWagonAndKeywordOnUser(wagon.getId(),recherche);
		
		if (!suiviWagons.isEmpty())
		for (SuiviWagon suiviWagon : suiviWagons) {
			followers.add(suiviWagon.getUser());
		}
		
		List<Utilisateur> unFollowers =  new ArrayList<>();
		//List<Utilisateur> utilisateurs = userDao.findByEntreprise(wagon.getEntreprise());
		List<Utilisateur> utilisateurs = userDao.findByKeywordAndEntreprise(recherche,wagon.getEntreprise().getId());
		
		if (!utilisateurs.isEmpty())
		for (Utilisateur utilisateur : utilisateurs) {
			if (!followers.contains(utilisateur)) unFollowers.add(utilisateur);
		}
		
		return new WagonAllUsers(followers, unFollowers);
	}
	
	@Override
	public  EntityPage<Wagon> pairingWagon(Utilisateur userConnected, WagonBoitier wagonBoitier) {
		
		 Boitier boitier = boitierDao.findById(wagonBoitier.getBoitier().getId()).get();
		 Wagon wagon = 
				 wagonBoitier.isPairing() ? findById(wagonBoitier.getWagon().getId()) : boitier.getWagon();
		
		if (userConnected.getRole() == UserRoles.SuperAdministrateur)
			throw new NotAuthorizedException();
		
		if (userConnected.getRole() == UserRoles.Administrateur) {
			// S'assurer que le wagon et le boitier sont de son entreprise
			if (wagon.getEntreprise().getId() != userConnected.getEntreprise().getId())
				throw new NotAuthorizedException();
			if (boitier.getEntreprise().getId() != userConnected.getEntreprise().getId())
				throw new NotAuthorizedException();			
		} else {
			// Vérifier que le wagon est suivi par l'utilisateur connecté
			SuiviWagon suiviWagon = suiviWagonDao.findByUserIdAndWagonId(userConnected.getId(), wagon.getId());
			if (suiviWagon == null)
				throw new NotAuthorizedException();

			// Vérifier que le boitier est de son entreprise
			if (boitier.getEntreprise().getId() != userConnected.getEntreprise().getId())
				throw new NotAuthorizedException();
		}
		
		if(wagonBoitier.isPairing()) { // Demande de pairing
					
			if (boitier.getEtat() == PairingBox.Paired)
				 throw new ErrorException("Ce boitier est déjà rattaché à un wagon");
			else {
				// Faire le pairing
				boitier.setWagon(wagon);
				boitier.setEtat(PairingBox.Paired);
				boitier.setSellette(wagonBoitier.getSellette());
				// Mettre à jour la date d'installation du boitier
				boitier.setDateInstallation(new Date());
				boitier = boitierDao.save(boitier);
				boitierDao.refresh(boitier);
				wagonDao.refresh(wagon);
				
				
				// Sauvegarder un Timeline du Boitier
				timelineBoxDao.save(
						new TimelineBox(new Date(), PairingBox.Paired, userConnected, wagon, boitier)
						);
				
			}
		} else { // Demande d'unpairing

			if (boitier.getEtat() != PairingBox.Paired)
				 throw new ErrorException("Ce boitier n'est pas rattaché à un wagon");
			else {
				// Faire le UnPairing
				boitier.setWagon(null);
				boitier.setEtat(PairingBox.Unpaired);
				boitier.setSellette(null);
				// Mettre à jour la date d'installation du boitier
				boitier.setDateInstallation(null);
				boitier = boitierDao.save(boitier);
				boitierDao.refresh(boitier);
				wagonDao.refresh(wagon);
				
				// Sauvegarder un Timeline du Boitier
				timelineBoxDao.save(
						new TimelineBox(new Date(), PairingBox.Unpaired, userConnected, wagon, boitier)
						);
			}
		}
		
		Pageable pageable = PageRequest.of(0, 20, Sort.by("serie").ascending());
		
		return findByKeyword(userConnected, "", pageable);
		
	}
	

}
