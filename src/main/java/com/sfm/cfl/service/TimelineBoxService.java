package com.sfm.cfl.service;

import java.util.List;

import com.sfm.cfl.model.TimelineBox;
import com.sfm.cfl.model.Utilisateur;

public interface TimelineBoxService {
	List<TimelineBox> findAllByBoitierId(Utilisateur userConnected, long boitierId);
}
