package com.sfm.cfl.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sfm.cfl.dto.mesures.CountParBoitier;
import com.sfm.cfl.model.Entreprise;
import com.sfm.cfl.model.Mesure;
import com.sfm.cfl.model.Utilisateur;
import com.sfm.cfl.myrepo.MyRepository;

public interface MesureDao extends MyRepository<Mesure, Long> {
    Page<Mesure> findByBoitierIdAndIndicateurId(long boitier_id, long indicateur_id, Pageable pageable);
    List<Mesure> findByBoitierIdAndIndicateurIdAndDateBetweenOrderByDate(long boitier_id, long indicateur_id, Date debut, Date fin);
    Mesure findTopByBoitierIdAndIndicateurIdOrderByIdDesc(long boitier_id, long indicateur_id);
    
//    @Query("SELECT new com.sfm.cfl.dto.mesures.MesureDTOVal(" 
//    		+ "m.date as date, " 
//    		+ "m.val1 as val1, "
//			+ "m.val2 as val2, " 
//    		+ "m.val3 as val3 ) "
//    		+ "FROM Mesure m " + "WHERE "
//    				+ "m.boitier.id = :idBoitier "
//    				+ "AND m.indicateur.id = :idIndicateur "
//    				+ "AND m.date between :startDate AND :endDate")
//	public List<MesureDTOVal> findByBoitierAndIndicateur(
//			@Param("idBoitier") long idBoitier,
//			@Param("idIndicateur") long idIndicateur, 
//			@Param("startDate") Date startDate,
//			@Param("endDate") Date endDate);
    
    @Query("SELECT m.datems as date, m.val1 as valeur "
    		+ "FROM Mesure m " + "WHERE "
    				+ "m.boitier.id = :idBoitier "
    				+ "AND m.indicateur.id = :idIndicateur "
    				+ "AND m.date between :startDate AND :endDate")
	public List<?> findByBoitierAndIncateurVal1(
			@Param("idBoitier") long idBoitier,
			@Param("idIndicateur") long idIndicateur, 
			@Param("startDate") Date startDate,
			@Param("endDate") Date endDate);
    
    @Query("SELECT m.datems as date, m.val2 as valeur "
    		+ "FROM Mesure m " + "WHERE "
    				+ "m.boitier.id = :idBoitier "
    				+ "AND m.indicateur.id = :idIndicateur "
    				+ "AND m.date between :startDate AND :endDate")
	public List<?> findByBoitierAndIncateurVal2(
			@Param("idBoitier") long idBoitier,
			@Param("idIndicateur") long idIndicateur, 
			@Param("startDate") Date startDate,
			@Param("endDate") Date endDate);  
    
    @Query("SELECT m.datems as date, m.val3 as valeur "
    		+ "FROM Mesure m " + "WHERE "
    				+ "m.boitier.id = :idBoitier "
    				+ "AND m.indicateur.id = :idIndicateur "
    				+ "AND m.date between :startDate AND :endDate")
	public List<?> findByBoitierAndIncateurVal3(
			@Param("idBoitier") long idBoitier,
			@Param("idIndicateur") long idIndicateur, 
			@Param("startDate") Date startDate,
			@Param("endDate") Date endDate);  
    
    
    @Query("SELECT  new com.sfm.cfl.dto.mesures.CountParBoitier("
			+ "count(*) as count, "
			+ "m.boitier.reference as reference, "
			+ "m.boitier.imei as imei "
			+ ") "
		+ "FROM Mesure m WHERE "
			+ "m.date between :dateDebut AND :dateFin "
			+ "GROUP BY m.boitier"
			)
	public List<CountParBoitier> countParBoitier(
		@Param("dateDebut") Date dateDebut,
		@Param("dateFin") Date dateFin);
    
    @Query("SELECT  new com.sfm.cfl.dto.mesures.CountParBoitier("
			+ "count(*) as count, "
			+ "m.boitier.reference as reference, "
			+ "m.boitier.imei as imei "
			+ ") "
		+ "FROM Mesure m WHERE "
			+ "m.boitier.entreprise = :entreprise "
			+ "AND m.date between :dateDebut AND :dateFin "
			+ "GROUP BY m.boitier"
			)
	public List<CountParBoitier> countParBoitierAndEntreprise(
		@Param("entreprise") Entreprise entreprise,
		@Param("dateDebut") Date dateDebut,
		@Param("dateFin") Date dateFin);
    
    @Query("SELECT  new com.sfm.cfl.dto.mesures.CountParBoitier("
			+ "count(*) as count, "
			+ "m.boitier.reference as reference, "
			+ "m.boitier.imei as imei "
			+ ") "
		+ "FROM Mesure m WHERE "
			+ "m.boitier.wagon.id IN "
				+ "(Select s.wagon.id from SuiviWagon s where s.user = :userConnected) "
			+ "AND m.date between :dateDebut AND :dateFin "
			+ "GROUP BY m.boitier"
			)
	public List<CountParBoitier> countParBoitierForUser(
		@Param("userConnected") Utilisateur userConnected,
		@Param("dateDebut") Date dateDebut,
		@Param("dateFin") Date dateFin);
    
    
    
    
    
}
