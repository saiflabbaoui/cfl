package com.sfm.cfl.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sfm.cfl.enumer.PairingBox;
import com.sfm.cfl.model.Boitier;
import com.sfm.cfl.myrepo.MyRepository;

@Repository
public interface BoitierDao extends MyRepository<Boitier, Long> {
	Page<Boitier> findByReferenceContainingOrImeiContainingOrWagonSerieContaining(
			String reference,String imei, String wagonSerie, Pageable pageable);
	
	@Query("SELECT b from Boitier b WHERE "
    		+ "(b.etat = :etat) "
    		+ "AND ("
			+ "b.reference LIKE CONCAT('%',:keyword,'%') "
			+ "OR b.imei LIKE CONCAT('%',:keyword,'%') "
			+ ")"
			)
	Page<Boitier> findByKeywordAndEtat(
			@Param("keyword") String keyword,
			@Param("etat") PairingBox etat,
			Pageable pageable);
	
	@Query("SELECT b from Boitier b WHERE "
    		+ "(b.entreprise.id = :entrepriseId) "
    		+ "AND ("
			+ "b.reference LIKE CONCAT('%',:keyword,'%') "
			+ "OR b.imei LIKE CONCAT('%',:keyword,'%') "
			+ ")"
			)
	Page<Boitier> findByKeywordAndEntreprise(
			@Param("keyword") String keyword,
			@Param("entrepriseId") long entrepriseId,
			Pageable pageable);
	
	@Query("SELECT b from Boitier b WHERE "
    		+ "(b.entreprise.id = :entrepriseId) "
    		+ "AND (b.etat = :etat) "
    		+ "AND ("
			+ "b.reference LIKE CONCAT('%',:keyword,'%') "
			+ "OR b.imei LIKE CONCAT('%',:keyword,'%') "
			+ ")"
			)
	Page<Boitier> findByKeywordAndEntrepriseAndEtat(
			@Param("keyword") String keyword,
			@Param("etat") PairingBox etat,
			@Param("entrepriseId") long entrepriseId,
			Pageable pageable);
	
	Boitier findByImei(String imei);
	
}
