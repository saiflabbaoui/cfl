package com.sfm.cfl.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.sfm.cfl.model.Entreprise;
import com.sfm.cfl.model.Wagon;
import com.sfm.cfl.myrepo.MyRepository;

@Repository
public interface WagonDao extends MyRepository<Wagon, Long> {
	Page<Wagon> findBySerieContaining(String keyword, Pageable pageable);
	Page<Wagon> findByEntrepriseAndSerieContaining(Entreprise entreprise, String keyword, Pageable pageable);
}
