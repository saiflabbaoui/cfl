package com.sfm.cfl.dao;

import java.util.List;

import com.sfm.cfl.model.TimelineBox;
import com.sfm.cfl.myrepo.MyRepository;

public interface TimelineBoxDao extends MyRepository<TimelineBox, Long> {
	List<TimelineBox> findAllByBoitierId(long wagonId);
}
