package com.sfm.cfl.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sfm.cfl.dto.mesures.Alarme;
import com.sfm.cfl.dto.mesures.AlarmeParBoitier;
import com.sfm.cfl.model.Boitier;
import com.sfm.cfl.model.Entreprise;
import com.sfm.cfl.model.Etat;
import com.sfm.cfl.model.Utilisateur;
import com.sfm.cfl.myrepo.MyRepository;

public interface EtatDao extends MyRepository<Etat, Long> {
	Etat findTopByBoitierOrderByIdDesc(Boitier boitier);
	long countByResoluAndEtatAlarme(boolean resolu, boolean isAlarme);
	long countByBoitierEntrepriseAndResoluAndEtatAlarme(Entreprise entreprise, boolean resolu, boolean isAlarme);

	@Query("SELECT new com.sfm.cfl.dto.mesures.Alarme("
			+ "e.id as id, " 
			+ "e.date as date, " 
			+ "e.etat.libelle as libelle, "
			+ "e.etat.criticite as criticite, "
			+ "e.resolu as resolu "
			+ ") "
		+ "FROM Etat e WHERE "
			+ "e.resolu = :resolu "
			+ "AND e.etat.alarme = :alarme "
			+ "Order by e.date desc"
			)
	public List<Alarme> allAlarmes( 
		@Param("resolu") Boolean resolu,
		@Param("alarme") boolean alarme);
	
	@Query("SELECT  new com.sfm.cfl.dto.mesures.AlarmeParBoitier("
			+ "e.id as id, " 
			+ "e.date as date, " 
			+ "e.etat.libelle as libelle, "
			+ "e.etat.criticite as criticite, "
			+ "e.resolu as resolu, "
			+ "e.boitier.reference as refBoitier, "
			+ "e.boitier.wagon.serie as serieWagon "
			+ ") "
		+ "FROM Etat e WHERE "
			+ "e.id IN ( "
			+ "SELECT MAX(ee.id) "
				+ "FROM Etat ee "
				+ " Where ( "
					+ "ee.resolu = :resolu "
					+ "AND ee.etat.alarme = :alarme "
				+ ") "
				+ "GROUP BY ee.boitier"
			+ ")"
			)
	public List<AlarmeParBoitier> lastAlarmes( 
		@Param("resolu") Boolean resolu,
		@Param("alarme") boolean alarme);

	@Query("SELECT new com.sfm.cfl.dto.mesures.Alarme("
			+ "e.id as id, " 
			+ "e.date as date, " 
			+ "e.etat.libelle as libelle, "
			+ "e.etat.criticite as criticite, "
			+ "e.resolu as resolu "
			+ ") "
		+ "FROM Etat e WHERE "
			+ "e.boitier.entreprise = :entreprise " 
			+ "AND e.resolu = :resolu "
			+ "AND e.etat.alarme = :alarme "
			+ "Order by e.date desc"
			)
	public List<Alarme> alarmesByEntreprise(
		@Param("entreprise") Entreprise entreprise, 
		@Param("resolu") Boolean resolu,
		@Param("alarme") boolean alarme);
	
	@Query("SELECT  new com.sfm.cfl.dto.mesures.AlarmeParBoitier("
			+ "e.id as id, " 
			+ "e.date as date, " 
			+ "e.etat.libelle as libelle, "
			+ "e.etat.criticite as criticite, "
			+ "e.resolu as resolu, "
			+ "e.boitier.reference as refBoitier, "
			+ "e.boitier.wagon.serie as serieWagon "
			+ ") "
		+ "FROM Etat e WHERE "
			+ "e.id IN ( "
			+ "SELECT MAX(ee.id) "
				+ "FROM Etat ee "
				+ " Where ( "
					+ "ee.boitier.entreprise = :entreprise "
					+ "AND ee.resolu = :resolu "
					+ "AND ee.etat.alarme = :alarme "
				+ ") "
				+ "GROUP BY ee.boitier"
			+ ")"
			)
	public List<AlarmeParBoitier> lastAlarmesByEntreprise(
		@Param("entreprise") Entreprise entreprise, 
		@Param("resolu") Boolean resolu,
		@Param("alarme") boolean alarme);
	
	@Query("SELECT new com.sfm.cfl.dto.mesures.Alarme("
				+ "e.id as id, " 
				+ "e.date as date, " 
				+ "e.etat.libelle as libelle, "
				+ "e.etat.criticite as criticite, "
				+ "e.resolu as resolu "
				+ ") "
			+ "FROM Etat e WHERE "
				+ "e.boitier.id = :idBoitier " 
				+ "AND e.date between :startDate AND :endDate "
				+ "Order by e.date desc"
				)
	public List<Alarme> findByBoitier(
			@Param("idBoitier") long idBoitier, 
			@Param("startDate") Date startDate,
			@Param("endDate") Date endDate);
	
	@Query("SELECT new com.sfm.cfl.dto.mesures.Alarme(" 
			+ "e.id as id, "
			+ "e.date as date, " 
			+ "e.etat.libelle as libelle, "
			+ "e.etat.criticite as criticite, "
			+ "e.resolu as resolu "
			+ ") "
		+ "FROM Etat e WHERE "
			+ "e.boitier.id = :idBoitier "
			+ "AND e.etat.alarme is true " 
			+ "AND e.date between :startDate AND :endDate "
			+ "AND e.resolu = :resolu "
			)
	public List<Alarme> findAlarmesByDate (
		@Param("idBoitier") long idBoitier,
		@Param("startDate") Date startDate,
		@Param("endDate") Date endDate,
		@Param("resolu") Boolean resolu);
	
	@Query("SELECT new com.sfm.cfl.dto.mesures.Alarme(" 
			+ "e.id as id, "
			+ "e.date as date, " 
			+ "e.etat.libelle as libelle, "
			+ "e.etat.criticite as criticite, "
			+ "e.resolu as resolu "
			+ ") "
		+ "FROM Etat e WHERE "
			+ "e.boitier.id = :idBoitier "
			+ "AND e.etat.alarme is true "
			+ "AND e.resolu = :resolu "
			+ "Order by e.date desc"
			)
	public List<Alarme> findAllAlarmes (
			@Param("idBoitier") long idBoitier,
			@Param("resolu") Boolean resolu);
	
	
	@Query("Select count(*) from Etat e "
			+ "where "
			+ "e.resolu = :resolu "
			+ "AND e.etat.alarme = :alarme "
			+ "AND e.boitier.wagon.id IN "
			+ "(Select s.wagon.id from SuiviWagon s where s.user = :userConnected) "
			+ "Order by e.date desc"
			)
	public long countUserAlarme(
			@Param("userConnected") Utilisateur userConnected,
			@Param("resolu") Boolean resolu,
			@Param("alarme") boolean alarme
			);
	
	@Query("SELECT new com.sfm.cfl.dto.mesures.Alarme(" 
			+ "e.id as id, "
			+ "e.date as date, " 
			+ "e.etat.libelle as libelle, "
			+ "e.etat.criticite as criticite, "
			+ "e.resolu as resolu "
			+ ") "
		+ "FROM Etat e WHERE "
			+ "e.resolu = :resolu "
			+ "AND e.etat.alarme = :alarme "
			+ "AND e.boitier.wagon.id IN "
			+ "(Select s.wagon.id from SuiviWagon s where s.user = :userConnected) "
			+ "Order by e.date desc"
			)
	public List<Alarme> userAlarme(
			@Param("userConnected") Utilisateur userConnected,
			@Param("resolu") Boolean resolu,
			@Param("alarme") boolean alarme
			);
	
	@Query("SELECT  new com.sfm.cfl.dto.mesures.AlarmeParBoitier("
			+ "e.id as id, " 
			+ "e.date as date, " 
			+ "e.etat.libelle as libelle, "
			+ "e.etat.criticite as criticite, "
			+ "e.resolu as resolu, "
			+ "e.boitier.reference as refBoitier, "
			+ "e.boitier.wagon.serie as serieWagon "
			+ ") "
		+ "FROM Etat e WHERE "
			+ "e.id IN ( "
			+ "SELECT MAX(ee.id) "
				+ "FROM Etat ee "
				+ " Where ( "
					+ "ee.resolu = :resolu "
					+ "AND ee.etat.alarme = :alarme "
					+ "AND ee.boitier.wagon.id IN "
					+ "(Select s.wagon.id from SuiviWagon s where s.user = :userConnected) "
				+ ") "
				+ "GROUP BY ee.boitier"
			+ ")"
			)
	public List<AlarmeParBoitier> lastUserAlarme(
			@Param("userConnected") Utilisateur userConnected,
			@Param("resolu") Boolean resolu,
			@Param("alarme") boolean alarme
			);

}
