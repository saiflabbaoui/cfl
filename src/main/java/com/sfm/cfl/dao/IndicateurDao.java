package com.sfm.cfl.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.sfm.cfl.model.Indicateur;
import com.sfm.cfl.myrepo.MyRepository;

@Repository
public interface IndicateurDao extends MyRepository<Indicateur, Long> {
	Page<Indicateur> findByLibelleContaining(String keyword, Pageable pageable);
}
