package com.sfm.cfl.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.sfm.cfl.model.Entreprise;
import com.sfm.cfl.myrepo.MyRepository;

@Repository
public interface EntrepriseDao extends MyRepository<Entreprise, Long> {
	Page<Entreprise> findByNomContaining(String keyword, Pageable pageable);
}
