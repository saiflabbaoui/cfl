package com.sfm.cfl.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.sfm.cfl.model.Boitier;
import com.sfm.cfl.model.Parametre;
import com.sfm.cfl.model.ValeurParametre;
import com.sfm.cfl.myrepo.MyRepository;

@Repository
public interface ValeurParametreDao extends MyRepository<ValeurParametre, Long> {
	List<ValeurParametre> findByBoitier(Boitier boitier);
	ValeurParametre findByParametreAndBoitier(Parametre parametre, Boitier boitier);
	ValeurParametre findByParametreIdAndBoitier(long paramId, Boitier boitier);
}
