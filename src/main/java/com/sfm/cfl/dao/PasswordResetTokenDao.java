package com.sfm.cfl.dao;

import com.sfm.cfl.model.PasswordResetToken;
import com.sfm.cfl.myrepo.MyRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PasswordResetTokenDao extends MyRepository<PasswordResetToken, Long> {
    PasswordResetToken findByToken(String token);
}
