package com.sfm.cfl.dao;

import com.sfm.cfl.model.TypeEtat;
import com.sfm.cfl.myrepo.MyRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface TypeEtatDao extends MyRepository<TypeEtat, Long> {
    Page<TypeEtat> findByLibelleContainingAndDescriptionContaining(String libelle, String description, Pageable pageable);
}

