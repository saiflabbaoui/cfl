package com.sfm.cfl.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.sfm.cfl.model.Parametre;
import com.sfm.cfl.myrepo.MyRepository;

@Repository
public interface ParametreDao extends MyRepository<Parametre, Long> {
	Page<Parametre> findByAttributContaining(String keyword, Pageable pageable);
}
