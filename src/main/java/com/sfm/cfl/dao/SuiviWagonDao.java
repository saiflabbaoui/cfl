package com.sfm.cfl.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sfm.cfl.model.SuiviWagon;
import com.sfm.cfl.model.Utilisateur;
import com.sfm.cfl.model.Wagon;
import com.sfm.cfl.myrepo.MyRepository;

public interface SuiviWagonDao extends MyRepository<SuiviWagon, Long> {
    SuiviWagon findByUserIdAndWagonId(long user_id, long wagon_id);
    Page<SuiviWagon> findByUserIdAndWagonSerieContaining(long user_id, String recherche, Pageable pageable);
    List<SuiviWagon> findByWagon(Wagon wagon);
    List<SuiviWagon> findByUser(Utilisateur user);
    
    @Query("SELECT s from SuiviWagon s WHERE "
    		+ "(s.wagon.id = :wagonId) "
    		+ "AND ( "
			+ "s.user.lastname LIKE CONCAT('%',:keyword,'%') "
			+ "OR s.user.firstname LIKE CONCAT('%',:keyword,'%') "
			+ "OR s.user.phone LIKE CONCAT('%',:keyword,'%') "
			+ "OR s.user.email LIKE CONCAT('%',:keyword,'%') "
			+ ")")
	List<SuiviWagon> findByWagonAndKeywordOnUser(
			@Param("wagonId") long wagonId,
			@Param("keyword") String keyword
			);
}
