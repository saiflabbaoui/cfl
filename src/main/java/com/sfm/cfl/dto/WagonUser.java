package com.sfm.cfl.dto;

import com.sfm.cfl.model.Utilisateur;
import com.sfm.cfl.model.Wagon;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class WagonUser {

	private Wagon wagon;
	private Utilisateur user;
	private boolean follow;

}
