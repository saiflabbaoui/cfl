package com.sfm.cfl.dto;

import com.sfm.cfl.enumer.Sellette;
import com.sfm.cfl.model.Boitier;
import com.sfm.cfl.model.Wagon;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class WagonBoitier {
	private Wagon wagon;
	private Boitier boitier;
	private boolean pairing;
	private Sellette sellette;

	public WagonBoitier(Wagon wagon, Boitier boitier, boolean pairing, Sellette sellette) {
		super();
		this.wagon = wagon;
		this.boitier = boitier;
		this.pairing = pairing;
		this.sellette = sellette;
	}

}
