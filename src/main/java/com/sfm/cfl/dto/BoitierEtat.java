package com.sfm.cfl.dto;

import com.sfm.cfl.model.Boitier;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class BoitierEtat {

	Boitier boitier;
	String etat;
	long keepAlive;

	public BoitierEtat(Boitier boitier, String etat, long keepAlive) {
		super();
		this.boitier = boitier;
		this.etat = etat;
		this.keepAlive = keepAlive;
	}

}
