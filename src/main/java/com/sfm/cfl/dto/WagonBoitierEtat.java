package com.sfm.cfl.dto;

import java.util.ArrayList;
import java.util.List;

import com.sfm.cfl.model.Boitier;
import com.sfm.cfl.model.Entreprise;
import com.sfm.cfl.model.Wagon;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class WagonBoitierEtat {

	private long id;
	private String serie;
	private Entreprise entreprise;
	private List<BoitierEtat> boitierEtats = new ArrayList<>();

	public WagonBoitierEtat(Wagon wagon) {
		super();
		this.id = wagon.getId();
		this.serie = wagon.getSerie();
		this.entreprise = wagon.getEntreprise();

		if (!wagon.getBoitiers().isEmpty())
			for (Boitier boitier : wagon.getBoitiers()) {
				this.boitierEtats.add(new BoitierEtat(boitier, null, 0));
			}
	}

}
