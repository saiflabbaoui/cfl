package com.sfm.cfl.dto.mapper;

import org.springframework.stereotype.Component;

import com.sfm.cfl.dto.UserDto;
import com.sfm.cfl.model.Utilisateur;


@Component
public class UserMapper {

    public static UserDto toUserDto(Utilisateur user) {
    	UserDto udto =  new UserDto(user);
        udto.setPassword("");
    	return udto;
    }

}
