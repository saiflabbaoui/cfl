package com.sfm.cfl.dto;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RequestResultat {

	private long id;
	private Date dateDebut;
	private Date dateFin;
	
}
