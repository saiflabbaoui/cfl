package com.sfm.cfl.dto.mesures;

import java.util.Date;

import com.sfm.cfl.enumer.Criticite;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AlarmeParBoitier {
	
	private long id;
	private Date date;
	private String libelle;
	private Criticite criticite;
	private Boolean resolu;
	private String refBoitier;
	private String serieWagon;
	

	public AlarmeParBoitier(long id, Date date,String libelle, Criticite criticite, Boolean resolu,
			String refBoitier, String serieWagon) {
		super();
		this.id = id;
		this.libelle = libelle;
		this.date = date;
		this.criticite = criticite;
		this.resolu = resolu;
		this.refBoitier = refBoitier;
		this.serieWagon = serieWagon;
	}

}
