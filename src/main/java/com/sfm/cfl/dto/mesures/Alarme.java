package com.sfm.cfl.dto.mesures;

import java.util.Date;

import com.sfm.cfl.enumer.Criticite;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Alarme {
	
	private long id;
	private Date date;
	private String libelle;
	private Criticite criticite;
	private Boolean resolu;

	public Alarme(long id, Date date,String libelle, Criticite criticite, Boolean resolu) {
		super();
		this.id = id;
		this.libelle = libelle;
		this.date = date;
		this.criticite = criticite;
		this.resolu = resolu;
	}

}
