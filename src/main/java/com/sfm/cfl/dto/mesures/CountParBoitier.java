package com.sfm.cfl.dto.mesures;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CountParBoitier {

	private long count;
	private String reference;
	private String imei;

	public CountParBoitier(long count, String reference, String imei) {
		super();
		this.count = count;
		this.reference = reference;
		this.imei = imei;
	}

}
