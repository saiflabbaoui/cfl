package com.sfm.cfl.dto.mesures;

import java.util.ArrayList;
import java.util.List;

import com.sfm.cfl.model.Indicateur;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class IndicateurMesures {

	private Indicateur indicateur;
	private List<MesureDTOVal> mesures = new ArrayList<>();

	public IndicateurMesures(Indicateur indicateur, List<MesureDTOVal> mesures) {
		super();
		this.indicateur = indicateur;
		this.mesures = mesures;
	}

}
