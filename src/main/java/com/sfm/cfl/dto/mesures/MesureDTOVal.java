package com.sfm.cfl.dto.mesures;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class MesureDTOVal {
	
	private String name;
	private List<?> data = new ArrayList<>();


	public MesureDTOVal(List<?> data,String name) {
		super();
		this.data = data;
		this.name = name;
	}

}
