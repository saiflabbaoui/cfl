package com.sfm.cfl.dto.apicfl;

import com.sfm.cfl.enumer.Sellette;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Box {
	private String reference;
	private Sellette sellette;

	public Box(String reference, Sellette sellette) {
		super();
		this.reference = reference;
		this.sellette = sellette;
	}

	@Override
	public String toString() {
		return "Box [reference=" + reference + ", sellette=" + sellette + "]";
	}

}
