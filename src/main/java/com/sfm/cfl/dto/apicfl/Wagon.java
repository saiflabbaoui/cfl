package com.sfm.cfl.dto.apicfl;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Wagon {
	private String serie;
	private String entreprise;

	public Wagon(String serie, String entreprise) {
		super();
		this.serie = serie;
		this.entreprise = entreprise;
	}

	@Override
	public String toString() {
		return "Wagon [serie=" + serie + ", entreprise=" + entreprise + "]";
	}

}
