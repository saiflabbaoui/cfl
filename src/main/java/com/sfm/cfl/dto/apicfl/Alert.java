package com.sfm.cfl.dto.apicfl;

import com.sfm.cfl.enumer.Criticite;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Alert {
	private Long id;
	private String label;
	private String valeur1;
	private String valeur2;
	private String valeur3;
	private Criticite criticite;
	private String createdAt;

	public Alert(Long id, String label, String valeur1, String valeur2, String valeur3, Criticite criticite, String createdAt) {
		super();
		this.id = id;
		this.label = label;
		this.valeur1 = valeur1;
		this.valeur2 = valeur2;
		this.valeur3 = valeur3;
		this.criticite = criticite;
		this.createdAt = createdAt;
	}

	@Override
	public String toString() {
		return "Alert [id=" + id + ", label=" + label + ", valeur1=" + valeur1 + ", valeur2=" + valeur2 + ", valeur3="
				+ valeur3 + ", criticite=" + criticite + ", createdAt=" + createdAt + "]";
	}

}
