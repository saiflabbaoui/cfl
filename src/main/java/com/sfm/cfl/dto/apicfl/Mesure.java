package com.sfm.cfl.dto.apicfl;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Mesure {
	private Long id;
	private String valeur1;
	private String valeur2;
	private String valeur3;
	private Date createdAt;

	public Mesure(Long id, String valeur1, String valeur2, String valeur3, Date createdAt) {
		super();
		this.id = id;
		this.valeur1 = valeur1;
		this.valeur2 = valeur2;
		this.valeur3 = valeur3;
		this.createdAt = createdAt;
	}

}
