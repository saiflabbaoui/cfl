package com.sfm.cfl.dto.apicfl;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ApiBody {
	private Alert alert;
	private Box box;
	private Wagon wagon;

	public ApiBody(Alert alert, Box box, Wagon wagon) {
		super();
		this.alert = alert;
		this.box = box;
		this.wagon = wagon;
	}

	@Override
	public String toString() {
		return "ApiBody [alert=" + alert + ", box=" + box + ", wagon=" + wagon + "]";
	}
	
	

}
