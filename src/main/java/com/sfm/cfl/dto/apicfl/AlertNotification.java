package com.sfm.cfl.dto.apicfl;

import java.util.Date;

import com.sfm.cfl.enumer.Criticite;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AlertNotification {
	private Long id;
	private boolean resolved;
	private Date resolvedAt;
	private Date type;
	private Date createdAt;
	private Criticite criticite;

	private Alert alert;
	private Mesure mesure;

	public AlertNotification(Long id, boolean resolved, Date resolvedAt, Date type, Date createdAt, Criticite criticite,
			Alert alert, Mesure mesure) {
		super();
		this.id = id;
		this.resolved = resolved;
		this.resolvedAt = resolvedAt;
		this.type = type;
		this.createdAt = createdAt;
		this.criticite = criticite;
		this.alert = alert;
		this.mesure = mesure;
	}

}
