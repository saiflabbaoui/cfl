package com.sfm.cfl.dto;

import com.sfm.cfl.model.Boitier;
import com.sfm.cfl.model.Entreprise;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class BoitierEntreprise {
	private Boitier boitier;
	private Entreprise entreprise;

	public BoitierEntreprise(Boitier boitier, Entreprise entreprise) {
		super();
		this.boitier = boitier;
		this.entreprise = entreprise;
	}

}
