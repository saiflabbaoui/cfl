package com.sfm.cfl.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class BoitierSim {

	private long idBoitier;
	private String sim;
	private boolean enable;

	public BoitierSim(long idBoitier, String sim) {
		super();
		this.idBoitier = idBoitier;
		this.sim = sim;
	}
	
	public BoitierSim(long idBoitier, boolean enable) {
		super();
		this.idBoitier = idBoitier;
		this.enable = enable;
	}

}
