package com.sfm.cfl.dto;

import lombok.Data;

@Data
public class PageUtil {

	private int numeroPage;
	private int nombreElementParPage;
	private int nombrePage;
	private long nombreTotalElement;

}
