package com.sfm.cfl.dto;

import java.util.List;

import com.sfm.cfl.model.Utilisateur;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class WagonAllUsers {

	private List<Utilisateur> followers;
	private List<Utilisateur> unFollowers;

	public WagonAllUsers(List<Utilisateur> followers, List<Utilisateur> unFollowers) {
		super();
		this.followers = followers;
		this.unFollowers = unFollowers;
	}

}
