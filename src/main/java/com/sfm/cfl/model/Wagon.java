package com.sfm.cfl.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Wagon {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(unique = true)
	private String serie;
	
	@ManyToOne
	@JoinColumn(name = "entreprise")
	private Entreprise entreprise;
	
	@JsonIgnoreProperties({"wagon", "entreprise"})
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany
	@JoinColumn(name = "wagon")
	private List<Boitier> boitiers = new ArrayList<>();
	
	@Override
	public String toString() {
		return serie + " ["
				+ "entreprise=" + entreprise.getNom()
				+ "]";
	}

}
