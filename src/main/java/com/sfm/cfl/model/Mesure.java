package com.sfm.cfl.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class Mesure {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column
	private Date date;
	@Column
	private long datems;
	@Column
	private String val1;
	@Column
	private String val2;
	@Column
	private String val3;

	@ManyToOne
	@JoinColumn(name = "boitier")
	private Boitier boitier;

	@ManyToOne
	@JoinColumn(name = "indicateur")
	private Indicateur indicateur;

	public Mesure(Date date, String val1, String val2, String val3, Boitier boitier, Indicateur indicateur) {
		super();
		this.date = date;
		this.val1 = val1;
		this.val2 = val2;
		this.val3 = val3;
		this.boitier = boitier;
		this.indicateur = indicateur;
	}

	@Override
	public String toString() {
		return "Mesure [id=" + id + ", date=" + date + ", val1=" + val1 + ", val2=" + val2 + ", val3=" + val3
				+ ", boitier=" + boitier + ", indicateur=" + indicateur + "]";
	}

}
