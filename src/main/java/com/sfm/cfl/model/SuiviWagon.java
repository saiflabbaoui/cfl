package com.sfm.cfl.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "wagon", "user"}))
public class SuiviWagon {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne
	@JoinColumn(name = "user")
	private Utilisateur user;

	@ManyToOne
	@JoinColumn(name = "wagon")
	private Wagon wagon;

	public SuiviWagon(Utilisateur user, Wagon wagon) {
		super();
		this.user = user;
		this.wagon = wagon;
	}
}
