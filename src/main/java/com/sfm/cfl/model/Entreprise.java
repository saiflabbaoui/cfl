package com.sfm.cfl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class Entreprise {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(unique = true)
	private String nom;
	
	@Column
	private String logo;
	
	@JsonIgnoreProperties({"entreprise"})
	@OneToOne
	@JoinColumn(name = "gestionnaire")
	private Utilisateur gestionnaire;
	
	public Entreprise(long id, String nom, String logo) {
		super();
		this.id = id;
		this.nom = nom;
		this.logo = logo;
	}
	
	@Override
	public String toString() {
		return nom + " ["
				+ ", logo=" + logo
				+ ", gestionnaire = " + gestionnaire.getFirstname() +" " + gestionnaire.getFirstname()
					+ "["
					+ "email=" + gestionnaire.getEmail()
					+ ", téléphone=" + gestionnaire.getPhone()
					+ "]"
				+ "]";
	}

}
