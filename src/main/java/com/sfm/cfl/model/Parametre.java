package com.sfm.cfl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class Parametre {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column
	private String attribut;
	
	@Column
	private String valeurParDefaut;
	
	@Column
	private String unite;
	
	@Override
	public String toString() {
		return attribut + " ["
				+ "valeur par défaut =" + valeurParDefaut
				+ ", unité =" + unite
				+ "]";
	}

}
