package com.sfm.cfl.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.sfm.cfl.enumer.PairingBox;
import com.sfm.cfl.enumer.Sellette;
import com.sfm.cfl.utils.Formatter;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class Boitier {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column
	private String imei;
	@Column
	private String reference;
	@Column
	private Date dateInstallation; // Date d'installation sur le dernier Wagon
	@Column
	private Date derniereConnection; // Date de dernière connection à la plateforme
	@Column
	private Date softLastUpdate; // Date de dernière mise à jour de son logiciel
	@Column
	private String versionSoft; // Date de dernière mise à jour de son logiciel
	@Column
	private boolean enable;
	@ManyToOne
	@JoinColumn(name = "wagon")
	private Wagon wagon;
	@ManyToOne
	@JoinColumn(name = "entreprise")
	private Entreprise entreprise;
	
	@Enumerated(EnumType.STRING)
	@Column
	private PairingBox etat;
	
	@Enumerated(EnumType.STRING)
	@Column
	private Sellette sellette;
	
	@Column
	private String sim;
	
	public Boitier(String imei, String reference, Date derniereConnection, boolean enable, PairingBox etat, String versionSoft) {
		super();
		this.imei = imei;
		this.reference = reference;
		this.derniereConnection = derniereConnection;
		this.enable = enable;
		this.etat = etat;
		this.versionSoft = versionSoft;
	}
	
	@Override
	public String toString() {
		return reference + " ["
				+ "actif=" + ((enable)  ? "oui" : "non")
				+ ", imei=" + imei
				+ ", installation=" + ( dateInstallation != null ? Formatter.formatDate(dateInstallation) : null )
				+ ", Dernière connexion=" + ( derniereConnection != null ? Formatter.formatDate(derniereConnection) : null )
				+ ", Mise à jour logiciel=" + ( softLastUpdate != null ? Formatter.formatDate(softLastUpdate) : null )
				+ ", entreprise=" + ( entreprise != null ? entreprise.getNom() : null )
				+ ", wagon=" + ( wagon != null ? wagon.getSerie() : null )
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Boitier other = (Boitier) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
