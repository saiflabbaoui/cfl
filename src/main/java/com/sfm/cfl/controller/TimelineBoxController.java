package com.sfm.cfl.controller;

import com.sfm.cfl.dto.ApiResponse;
import com.sfm.cfl.dto.EntityPage;
import com.sfm.cfl.model.TimelineBox;
import com.sfm.cfl.service.TimelineBoxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/timelineBox")
public class TimelineBoxController {

    @Autowired
    private TimelineBoxService timelineBoxService;

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ApiResponse saveTimelineBox(@RequestBody TimelineBox timelineBox) {
        TimelineBox retour =  timelineBoxService.save(timelineBox);
        return new ApiResponse(retour, "OK", HttpStatus.OK.value());
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ApiResponse listTimelineBox(
            @RequestParam(name = "numPage", defaultValue = "1", required = false) int numPage,
            @RequestParam(name = "nombreElement", defaultValue = "20", required = false) int nombreElement,
            @RequestParam(name = "user_id", required = true) long user_id,
            @RequestParam(name = "wagon_id", required = true) long wagon_id,
            @RequestParam(name = "boitier_id", required = true) long boitier_id) {

        Pageable pageable = PageRequest.of(numPage - 1, nombreElement, Sort.by("date").ascending());

        EntityPage<TimelineBox> retour = timelineBoxService.listTimelineBox(user_id, wagon_id, boitier_id, pageable);

        return new ApiResponse(retour, "OK", HttpStatus.OK.value());

    }

    @RequestMapping(value = "/details", method = RequestMethod.POST)
    public ApiResponse getById(@RequestBody long id) {
        TimelineBox retour =   timelineBoxService.findById(id);
        return new ApiResponse(retour, "OK", HttpStatus.OK.value());
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public ApiResponse deleteById(@RequestBody long id) {
        timelineBoxService.delete(id);
        return new ApiResponse("", "OK", HttpStatus.OK.value());
    }
}
