package com.sfm.cfl.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sfm.cfl.dto.ApiResponse;
import com.sfm.cfl.dto.RequestMesure;
import com.sfm.cfl.enumer.UserRoles;
import com.sfm.cfl.model.Utilisateur;
import com.sfm.cfl.service.EtatService;
import com.sfm.cfl.service.UserService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/etat")
public class EtatController {
	
	@Autowired
	private EtatService etatService;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/listByDate", method = RequestMethod.POST)
	public ApiResponse allEtatsByDate(@RequestBody RequestMesure requestMesure) {
    	
		return new ApiResponse(
				etatService.allEtatsByDate(
						requestMesure.getIdBoitier(),
						requestMesure.getDateDebut(), 
						requestMesure.getDateFin()
						),
				"OK", 
				HttpStatus.OK.value()
				);
    }
	
	@RequestMapping(value = "/alarmes", method = RequestMethod.POST)
	public ApiResponse allAlarmesByDate(@RequestBody RequestMesure requestMesure) {
    	
		return new ApiResponse(
				etatService.allAlarmesByDate(
						requestMesure.getIdBoitier(),
						requestMesure.getDateDebut(), 
						requestMesure.getDateFin(),
						requestMesure.isResolu()
						),
				"OK", 
				HttpStatus.OK.value()
				);
    }
	
	@RequestMapping(value = "/allAlarmes", method = RequestMethod.POST)
	public ApiResponse allAlarmes(@RequestBody boolean resolu) {
    	
		return new ApiResponse(
				etatService.allAlarmes(
						getConnectedUser(),
						resolu
						),
				"OK", 
				HttpStatus.OK.value()
				);
    }
	
	@RequestMapping(value = "/lastAlarmeParBox", method = RequestMethod.POST)
	public ApiResponse lastAlarmeParBox(@RequestBody boolean resolu) {
    	
		return new ApiResponse(
				etatService.lastAlarmeParBox(
						getConnectedUser(),
						resolu
						),
				"OK", 
				HttpStatus.OK.value()
				);
    }
	
	@RequestMapping(value = "/alarmeResolved", method = RequestMethod.POST)
	public ApiResponse alarmeResolved(@RequestBody long idEtat) {
    	
		return new ApiResponse(
				etatService.alarmeResolved(
						getConnectedUser(),
						idEtat
						),
				"OK", 
				HttpStatus.OK.value()
				);
    }
	
	@RequestMapping(value = "/alarmeCount", method = RequestMethod.GET)
	public ApiResponse alarmeCount() {
    	
		return new ApiResponse(
				etatService.alarmeCount(
						getConnectedUser()
						),
				"OK", 
				HttpStatus.OK.value()
				);
    }
	
	public boolean isSuperAdmin() {
		Utilisateur userConnected = getConnectedUser();
		if (userConnected.getRole()==UserRoles.SuperAdministrateur) {
			return true;
		}
		return false;
	}
	
	private Utilisateur getConnectedUser() {
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
		String email = userDetails.getUsername();
		return userService.findByEmail(email);
	}
	
}
