package com.sfm.cfl.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.sfm.cfl.dto.ApiResponse;
import com.sfm.cfl.dto.RequestMesure;
import com.sfm.cfl.dto.apicfl.Alert;
import com.sfm.cfl.dto.apicfl.ApiBody;
import com.sfm.cfl.dto.apicfl.Box;
import com.sfm.cfl.dto.apicfl.Wagon;
import com.sfm.cfl.dto.mesures.CountParBoitier;
import com.sfm.cfl.dto.mesures.MesureDTO;
import com.sfm.cfl.enumer.PairingBox;
import com.sfm.cfl.model.Boitier;
import com.sfm.cfl.model.Etat;
import com.sfm.cfl.model.Mesure;
import com.sfm.cfl.model.MesureBrut;
import com.sfm.cfl.model.TypeEtat;
import com.sfm.cfl.model.Utilisateur;
import com.sfm.cfl.model.ValeurParametre;
import com.sfm.cfl.service.BoitierService;
import com.sfm.cfl.service.EtatService;
import com.sfm.cfl.service.IndicateurService;
import com.sfm.cfl.service.MesureBrutService;
import com.sfm.cfl.service.MesureService;
import com.sfm.cfl.service.TypeEtatService;
import com.sfm.cfl.service.UserService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/mesure")
public class MesureController {

    @Autowired
    private MesureService mesureService;
    
    @Autowired
    private MesureBrutService mesureBrutService;
    
    @Autowired
    private IndicateurService indicateurService;
    
    @Autowired
    private TypeEtatService typeEtatService;
    
    @Autowired
    private EtatService etatService;
    
    @Autowired
    private BoitierService boitierService;
    
    @Value("${indic.KeepAlive}")
	private long indicKeepAlive;
	
    @Value("${indic.VersionHard}")
	private long indicVersionHardware;
    
    @Value("${indic.VersionSoft}")
	private long indicVersionSoftWare;
    
    @Value("${indic.EtatWagon}")
	private long indicEtatWagon;
    
    @Value("${paramid.flagMaj}")
	private long paramFlagMaj;
    
    @Value("${cfl_url}")
	private String cfl_url;
    
    @Value("${cfl_password}")
	private String cfl_password;
    
    @Value("${cfl_user}")
	private String cfl_user;
    
    @Value("${cfl_x_api_key}")
	private String cfl_x_api_key;
    
    @Autowired
	private UserService userService;
    
  
    @RequestMapping(value = "/adjust", method = RequestMethod.GET)
    public void adjust() {
    	List<Mesure> mesures = mesureService.findAll();
    	for (Mesure mesure : mesures) {
			mesure.setDatems(mesure.getDate().getTime());
			mesureService.save(mesure);
		}
    }
    
    @RequestMapping(value = "/updateOK", method = RequestMethod.GET)
    public String updateOK(@RequestParam(name = "imei", required = true) String imei) {
    	// Vérifier si le boitier est enregistré à travers l'imei
    	Boitier boitier = boitierService.findByImei(imei);
    	if (boitier == null) {
    		throw new EntityNotFoundException();
		}
		
    	// Mettre la valeur du flag de MAJ à ZERO
		ValeurParametre flagMAJ = boitierService.setBoitierParametre(boitier, paramFlagMaj, "0");
    	
    	// Retourner les paramètres du boitier
		List<ValeurParametre> vals = new ArrayList<ValeurParametre>();
		vals.add(flagMAJ);
        return formatParameters(vals);
    	
    }
    
    @RequestMapping(value = "/countEnvoiParBoitier", method = RequestMethod.POST)
    public ApiResponse countEnvoiParBoitier(@RequestBody RequestMesure requestMesure) {
    	List<CountParBoitier> retour =  mesureService.countEnvoiParBoitier(getConnectedUser(), requestMesure.getDateDebut(),requestMesure.getDateFin());
    	return new ApiResponse(retour, "OK", HttpStatus.OK.value());
    }
    

    @RequestMapping(value = "/save", method = RequestMethod.GET)
    public String saveMesure(
    		@RequestParam(name = "mesure", required = true) String mesureString
    		) {
    	
    	System.out.println("===== Mesure : "+ mesureString);
    	mesureBrutService.save(new MesureBrut(new Date(), mesureString));

    	/************************ Les paramètres sur lesquels on fera des traitements spécifiques ***********************/
    	List<MesureDTO> mesureDTOs = new ArrayList<>();
    	String versionHardware = "";
    	String versionSoftWare = "";
    	int keepAlive = 0;
    	
    	
    	String[] parties = mesureString.split(";");
    	
    	/************************ Collecte de l'imei ***********************/ 
    	String imei = parties[0];
    	System.out.println("====== imei : "+ imei);
    
    	/************************ Collecte des mesures ***********************/
    	for (int i = 1; i < parties.length ; i++) {
            System.out.println("--------->" + parties[i]);
            String[] indicateurParams = parties[i].split(",");
            MesureDTO mesureDTO = new MesureDTO(
            		indicateurParams[0], 
            		indicateurParams[1], 
            		indicateurParams[2], 
            		Long.parseLong(indicateurParams[3]));
            
            // Récupération du KeepAlive
            if ( mesureDTO.getIndicateur() == indicKeepAlive) {
				keepAlive = Integer.parseInt(mesureDTO.getVal1());
				continue; // ?????? enregistrer dans la BD ou pas ?
			}
            // Récupération de la verson HardWare
            if ( mesureDTO.getIndicateur() == indicVersionHardware) {
				versionHardware = mesureDTO.getVal1();
				continue;
			}
            // Récupération de la verson SoftWare
            if ( mesureDTO.getIndicateur() == indicVersionSoftWare) {
				versionSoftWare = mesureDTO.getVal1();
				continue;
			}
            mesureDTOs.add(mesureDTO); 
        }
    	
    	// Vérifier si le boitier est enregistré à travers l'imei
    	Boitier boitier = boitierService.findByImei(imei);
    	if (boitier == null) {
    		// Enregistrer un nouveau boitier
    		boitier = new Boitier(imei, "", new Date(), false, PairingBox.Manufactured, versionSoftWare);
    		boitierService.newBoitier(boitier,versionHardware);
    		
		} else {
			// Enregistrement de la mesure
			System.out.println(" Le boitier existe !!!!!!!");
			
	    	//Vérifier le KEEPAlive
			if (keepAlive == 1) {
			//	Si 1 : pas de changement d'état ==> pas d'envoi à CFL ==> pas d'enregistrement de mesure
			//		Mise à jour date de dernière connexion boitier
			//		Mise à jour version soft
				boitier.setDerniereConnection(new Date());
				boitier.setVersionSoft(versionSoftWare); // ????
				boitier = boitierService.saveBoitier(boitier);
				
			} else {
	    	//	Si 0 : Nouvelle mesure
	    	//		Mise à jour date de dernière connexion boitier
				boitier.setDerniereConnection(new Date());
				boitier.setVersionSoft(versionSoftWare); // ????
				boitier = boitierService.saveBoitier(boitier);
				// Enregistrer les mesures
				if (!mesureDTOs.isEmpty())
				for (MesureDTO mes : mesureDTOs) {
					try {
						
						Mesure mesure = new Mesure(
								new Date(), 
								mes.getVal1(), 
								mes.getVal2(), 
								mes.getVal3(), 
								boitier, 
								indicateurService.findById(mes.getIndicateur()));
						mesure.setDatems(mesure.getDate().getTime());
						
						mesure = mesureService.save(mesure);
						
						System.out.println("========> Mesuuuuuure : " + mesure);
						
						// Récupération de l'état WAGON
			            if (mesure.getIndicateur().getId() == indicEtatWagon) {
			            	System.out.println("========> indic Etat Wagon : " + mesure);
							// Enrégistrer un état
			            	TypeEtat typeEtat = typeEtatService.findById(Long.parseLong(mesure.getVal1()));
			            	Etat etat = new Etat(
			            			new Date(),
			            			typeEtat,
			            			boitier,
			            			// Générer une alerte en fonction de l'état
			            			(typeEtat.isAlarme())? false : null
			            			);
			            	etat = etatService.save(etat);
			            	
			            	// Envoyer l'etat à CFL (API)
			            	//Prendre en considération le paramètre Enable du boitier pour envoyer des alertes ou pas à CFL
			            	
			            	if(boitier.isEnable()){
			            		ApiBody apiBody = new ApiBody(
				            			new Alert(
				            					etat.getId(),
				            					etat.getEtat().getLibelle(),
				            					mesure.getVal1(),
				            					mesure.getVal2(),
				            					mesure.getVal3(),
				            					etat.getEtat().getCriticite(),
				            					dateToString(etat.getDate())),
				            			new Box(
				            					etat.getBoitier().getReference(),
				            					etat.getBoitier().getSellette()), 
				            			new Wagon(
				            					etat.getBoitier().getWagon() == null ? null : etat.getBoitier().getWagon().getSerie(),
				            					etat.getBoitier().getWagon() == null ? null : etat.getBoitier().getWagon().getEntreprise().getNom())
				            			);
				            	
				            	System.out.println("========> apibody : " + apiBody);
				            	
				            	sendCFLAlert(apiBody);
			            	}
			            	
			            	
						}
						
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				}
				
			}
			
		}
    	
    	// Retourner les paramètres du boitier
        return formatParameters(boitierService.findBoitierParametre(boitier));
    }
    
	@RequestMapping(value = "/mesureByDate", method = RequestMethod.POST)
	public ApiResponse mesureByDate(@RequestBody RequestMesure requestMesure) {
    	
		return new ApiResponse(
				mesureService.listMesureByDate(
						requestMesure.getIdBoitier(), 
						requestMesure.getIdIndicateur(),
						requestMesure.getDateDebut(), 
						requestMesure.getDateFin()),
				"OK", 
				HttpStatus.OK.value()
				);
		
    }
	
	@RequestMapping(value = "/lastMesures", method = RequestMethod.POST)
	public ApiResponse lastMesures(@RequestBody RequestMesure requestMesure) {
    	
		return new ApiResponse(
				mesureService.lastMesures(
						requestMesure.getIdBoitier()),
				"OK", 
				HttpStatus.OK.value()
				);
		
    }
	
	@RequestMapping(value = "/allMesuresByDate", method = RequestMethod.POST)
	public ApiResponse allMesuresByDate(@RequestBody RequestMesure requestMesure) {
    	
		return new ApiResponse(
				mesureService.allMesuresByDate(
						requestMesure.getIdBoitier(),
						requestMesure.getDateDebut(), 
						requestMesure.getDateFin()),
				"OK", 
				HttpStatus.OK.value()
				);
		
    }
	
	@RequestMapping(value = "/brutes", method = RequestMethod.POST)
	public ApiResponse mesuresBrutes(@RequestBody RequestMesure requestMesure) {
    	
		return new ApiResponse(
				mesureBrutService.brutesByDate(
						requestMesure.getDateDebut(), 
						requestMesure.getDateFin()),
				"OK", 
				HttpStatus.OK.value()
				);
		
    }
	
	public void sendCFLAlert(ApiBody apiBody) {

		RestTemplate restTemplate = new RestTemplate();
		
		// Headers
		HttpHeaders headers = new HttpHeaders();
		headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		headers.setBasicAuth(cfl_user,cfl_password);
		headers.add("x-api-key ", cfl_x_api_key);
		
		//POST
		HttpEntity<ApiBody> request = new HttpEntity<ApiBody>(apiBody, headers);

		ResponseEntity<String> response = restTemplate.postForEntity(
				cfl_url,
				request,
				String.class);
		
		System.out.println(" Réponse API : "
				+ "code " + response.getStatusCodeValue()
				+ ", body "+ response.getBody());
	}
	
	private Utilisateur getConnectedUser() {
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
		String email = userDetails.getUsername();
		return userService.findByEmail(email);
	}
    
    public String formatParameters(List<ValeurParametre> valeurParametres) {
    	String retour="";
    	if (!valeurParametres.isEmpty())
    	for (ValeurParametre valeurParametre : valeurParametres) {
			retour += valeurParametre.getValeur() + "," + valeurParametre.getParametre().getId() + ";" ;
		}
    	return retour;
    }
    
    public String dateToString(Date date) {
    	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	return dateFormat.format(date);  
    }

}
