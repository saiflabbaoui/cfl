package com.sfm.cfl.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sfm.cfl.dto.ApiResponse;
import com.sfm.cfl.dto.BoitierEtat;
import com.sfm.cfl.dto.EntityPage;
import com.sfm.cfl.dto.WagonAllUsers;
import com.sfm.cfl.dto.WagonBoitier;
import com.sfm.cfl.dto.WagonBoitierEtat;
import com.sfm.cfl.dto.WagonUser;
import com.sfm.cfl.enumer.UserRoles;
import com.sfm.cfl.exception.NotAuthorizedException;
import com.sfm.cfl.model.Utilisateur;
import com.sfm.cfl.model.Wagon;
import com.sfm.cfl.service.EtatService;
import com.sfm.cfl.service.ParametreService;
import com.sfm.cfl.service.UserService;
import com.sfm.cfl.service.WagonService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/wagon")
public class WagonController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private WagonService wagonService;
	
	@Autowired
	private EtatService etatService;
	
	@Autowired
	private ParametreService parametreService;
	
	@Value("${paramid.keepAlive}")
	private long paramKeepAlive;
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ApiResponse saveWagon(@RequestBody Wagon wagon) {
		
		if (!isAdmin()) throw new NotAuthorizedException();
		
		Utilisateur userConnected = getConnectedUser();
		Wagon retour =  wagonService.save(userConnected,wagon);
		return new ApiResponse(retour, "OK", HttpStatus.OK.value());
	}
	
	@RequestMapping(value = "/followers", method = RequestMethod.POST)
	public ApiResponse followers(@RequestBody Wagon wagon,
			@RequestParam(name = "recherche", defaultValue = "", required = false) String recherche) {
		if (!isAdmin()) throw new NotAuthorizedException();
		Utilisateur userConnected = getConnectedUser();
		WagonAllUsers retour =  wagonService.followers(userConnected,wagon,recherche);
		return new ApiResponse(retour, "OK", HttpStatus.OK.value());
	}
	
	@RequestMapping(value = "/follow", method = RequestMethod.POST)
	public ApiResponse followByUsers(@RequestBody WagonUser wagonUser) {
		if (!isAdmin()) throw new NotAuthorizedException();
		Utilisateur userConnected = getConnectedUser();
		WagonAllUsers retour;
		if (wagonUser.isFollow()) {
			retour =  wagonService.follow(userConnected,wagonUser.getWagon(),wagonUser.getUser());
		} else {
			retour =  wagonService.UnFollow(userConnected,wagonUser.getWagon(),wagonUser.getUser());
		}
		return new ApiResponse(retour, "OK", HttpStatus.OK.value());
	}
	
	@RequestMapping(value = "/pairing", method = RequestMethod.POST)
	public ApiResponse pairing(@RequestBody WagonBoitier wagonBoitier) {
		Utilisateur userConnected = getConnectedUser();
		EntityPage<Wagon> retour = wagonService.pairingWagon(userConnected, wagonBoitier);
		return new ApiResponse(retour, "OK", HttpStatus.OK.value());
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ApiResponse listWagon(
			@RequestParam(name = "numPage", defaultValue = "1", required = false) int numPage,
			@RequestParam(name = "nombreElement", defaultValue = "20", required = false) int nombreElement,
			@RequestParam(name = "recherche", defaultValue = "", required = false) String recherche) {
		
		Pageable pageable = PageRequest.of(numPage - 1, nombreElement, Sort.by("serie").ascending());
		
		Utilisateur userConnected = getConnectedUser();
		EntityPage<Wagon>  retour = wagonService.findByKeyword(userConnected,recherche, pageable);
		
		return new ApiResponse(retour, "OK", HttpStatus.OK.value());
		
	}
	
	@RequestMapping(value = "/listDash", method = RequestMethod.GET)
	public ApiResponse listWagonDash(
			@RequestParam(name = "numPage", defaultValue = "1", required = false) int numPage,
			@RequestParam(name = "nombreElement", defaultValue = "20", required = false) int nombreElement,
			@RequestParam(name = "recherche", defaultValue = "", required = false) String recherche) {
		
		Pageable pageable = PageRequest.of(numPage - 1, nombreElement, Sort.by("serie").ascending());
		
		Utilisateur userConnected = getConnectedUser();
		EntityPage<Wagon>  retourWagon = wagonService.findByKeyword(userConnected,recherche, pageable);
		
		EntityPage<WagonBoitierEtat>  retour = new EntityPage<>();
		
		retour.setPageUtil(retourWagon.getPageUtil());
		
		retour.setList(new ArrayList<>());		
		List<WagonBoitierEtat> wagonBoitierEtats = new ArrayList<>();
		if (!retourWagon.getList().isEmpty())
		for(Wagon wagon : retourWagon.getList()) {
			WagonBoitierEtat wagonBoitierEtat = new WagonBoitierEtat(wagon);
			for (BoitierEtat boitierEtat : wagonBoitierEtat.getBoitierEtats()) {
				boitierEtat.setEtat(
						// Dernier Etat du boitier
						(etatService.findByBoitier(boitierEtat.getBoitier())) != null
						?(etatService.findByBoitier(boitierEtat.getBoitier())).getEtat().getLibelle() : null
						);
				boitierEtat.setKeepAlive(
						// Valeur du paramètre
						Long.parseLong(parametreService.findParamValueByBoitier(paramKeepAlive, boitierEtat.getBoitier()).getValeur())
						);
			}
			
			wagonBoitierEtats.add(wagonBoitierEtat);
		}
		
		retour.setList(wagonBoitierEtats);
		
		return new ApiResponse(retour, "OK", HttpStatus.OK.value());
		
	}
	
	@RequestMapping(value = "/details", method = RequestMethod.POST)
	public ApiResponse getById(@RequestBody long id) {
		Wagon retour =   wagonService.findById(id);
		return new ApiResponse(retour, "OK", HttpStatus.OK.value());
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public ApiResponse deleteById(@RequestBody long id) {
		
		if (!isAdmin() && !isSuperAdmin()) throw new NotAuthorizedException();
		
		Utilisateur userConnected = getConnectedUser();
		wagonService.delete(userConnected,id);
		return new ApiResponse("", "OK", HttpStatus.OK.value());
	}
	
	public boolean isSuperAdmin() {
		Utilisateur userConnected = getConnectedUser();
		if (userConnected.getRole()==UserRoles.SuperAdministrateur) {
			return true;
		}
		return false;
	}
	
	public boolean isAdmin() {
		Utilisateur userConnected = getConnectedUser();
		if (userConnected.getRole()==UserRoles.Administrateur) {
			return true;
		}
		return false;
	}
	
	private Utilisateur getConnectedUser() {
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
		String email = userDetails.getUsername();
		return userService.findByEmail(email);
	}
	
}
