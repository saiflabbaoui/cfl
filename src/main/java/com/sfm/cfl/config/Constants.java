package com.sfm.cfl.config;

public class Constants {
    public static final long ACCESS_TOKEN_VALIDITY_SECONDS =  12 * 60 * 60;
    public static final String SIGNING_KEY = "cfl@Sfm2o21";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String TOKEN_ISSUER = "CFL";
    public static final String HEADER_STRING = "Authorization";
    public static final long PASSWORD_RESET_EXPIRATION_TIME = 900000;// 15 minutes
    
}
