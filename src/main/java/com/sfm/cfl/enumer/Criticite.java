package com.sfm.cfl.enumer;

public enum Criticite {
	Mineur,
	Majeur,
	Critique,
}