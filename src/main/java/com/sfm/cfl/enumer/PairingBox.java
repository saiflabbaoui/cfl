package com.sfm.cfl.enumer;

public enum PairingBox {
	Paired, 
	Unpaired, 
	Manufactured
}