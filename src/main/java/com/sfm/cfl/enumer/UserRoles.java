package com.sfm.cfl.enumer;

public enum UserRoles {
    Utilisateur,
    Administrateur,
    SuperAdministrateur
}